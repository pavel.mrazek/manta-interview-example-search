public interface BinarySearch {

    /**
     * Performs binary search of the given value in the input array.
     * The array must be sorted in an ascending order otherwise the algorithm is indeterminate.
     * @param value Looked up value.
     * @param array Array to be searched in. Not null.
     * @return The position of the value's first occurrence or -1 if not present.
     * @throws IllegalArgumentException if the input array is null.
     */
    int search(int value, int[] array);

}
