import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BinarySearchTest {

    private static BinarySearch algorithm;

    @Before
    public void setup(){
        algorithm = new BinarySearchImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchTestNullArray(){
        final int value = 0;
        final int position = algorithm.search(value, null);
    }

    @Test
    public void searchTestEmptyArray(){
        final int value = 0;
        final int position = algorithm.search(value, new int[]{});
        Assert.assertEquals("Incorrect position", -1, position);
    }

    @Test
    public void searchTestNotFound(){
        final int value = 0;
        final int[] array = {1,2,2};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", -1, position);
    }

    @Test
    public void searchTestNotFoundOneItemArray(){
        final int value = 0;
        final int[] array = {1};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", -1, position);
    }

    @Test
    public void searchTestFoundInOneItemArray(){
        final int value = -5;
        final int[] array = {-5};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", 0, position);
    }

    @Test
    public void searchTestFoundOnLastPositionInTwoItemsArray(){
        final int value = 3;
        final int[] array = {-1, 3};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", 1, position);
    }

    @Test
    public void searchTestFoundOnFirstPositionInTwoItemsArray(){
        final int value = -1;
        final int[] array = {-1, 3};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", 0, position);
    }

    @Test
    public void searchTestNotFoundInTwoItemsArray(){
        final int value = 4;
        final int[] array = {-1, 3};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", -1, position);
    }

    @Test
    public void searchTestFoundInThreeItemsArray(){
        final int value = -1;
        final int[] array = {-1, 4, 8};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", 0, position);
    }

    @Test
    public void searchTestFoundInBiggerArray(){
        final int value = 10897;
        final int[] array = {-2, 0, 2, 3, 4, 8, 156, 10897, 65468};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", 7, position);
    }

    @Test
    public void searchTestFoundInBiggerArrayOnLastPosition(){
        final int value = 10897;
        final int[] array = {-2, 0, 2, 3, 4, 8, 156, 10897};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", 7, position);
    }

    @Test
    public void searchTestNotFoundInBiggerArray(){
        final int value = 500;
        final int[] array = {-2, 0, 2, 3, 4, 8, 156, 10897};
        final int position = algorithm.search(value, array);
        Assert.assertEquals("Incorrect position", -1, position);
    }

}
